package com.spanner.googlespanner.wiremock;


import com.github.tomakehurst.wiremock.common.ConsoleNotifier;
import com.github.tomakehurst.wiremock.core.Options;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.spanner.googlespanner.constant.SpannerConstants;
import com.spanner.googlespanner.dto.InboxData;
import com.spanner.googlespanner.dto.OutboxData;
import com.spanner.googlespanner.dto.SampleTestBoxData;
import com.spanner.googlespanner.exception.DemoExceptionResponse;
import com.spanner.googlespanner.service.DemoService;
import com.spanner.googlespanner.spanner.SpannerDatabaseService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(properties = {"job.service.base.url=http://localhost:8088"})
public class GenericWiremockTests {

    private static final String GENERIC_RESPONSE_TRANSFORMER = "generic-response-transformer";
    private GenericResponseTransformer genericResponseTransformer = new GenericResponseTransformer();

    @Autowired
    private SpannerDatabaseService spannerDatabaseService;

    Options options = wireMockConfig().
            port(8088)
            .notifier(new ConsoleNotifier(true))
            .extensions(genericResponseTransformer);

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(options);

    @Autowired
    private DemoService demoService;

    @Test
    public void testGetOutboxData() throws DemoExceptionResponse {

        genericResponseTransformer.setSpannerDatabaseService(spannerDatabaseService);

        stubFor(get(anyUrl())
                .willReturn(aResponse()
                        .withTransformerParameter(SpannerConstants.TABLE_NAME, SpannerConstants.OUTBOX_TABLE_NAME)
                        .withTransformerParameter(SpannerConstants.DATABASE_NAME, SpannerConstants.DATABASE_ONE)
                        .withTransformers(GENERIC_RESPONSE_TRANSFORMER)));

        OutboxData outboxData = demoService.getOutboxData();
        log.info("outbox received : {}", outboxData);

        Assert.assertEquals(outboxData.getData(),"data");
    }

    @Test
    public void testGetInboxData() throws DemoExceptionResponse, IOException {

        genericResponseTransformer.setSpannerDatabaseService(spannerDatabaseService);

        stubFor(get(anyUrl())
                .willReturn(aResponse()
                        .withTransformerParameter(SpannerConstants.TABLE_NAME, SpannerConstants.INBOX_TABLE_NAME)
                        .withTransformerParameter(SpannerConstants.DATABASE_NAME, SpannerConstants.DATABASE_TWO)
                        .withTransformers(GENERIC_RESPONSE_TRANSFORMER)));

        InboxData inboxData = demoService.getInboxData();
        log.info("inboxData received : {}", inboxData);

        Assert.assertEquals(inboxData.getParentLoc(), "ABC001");
    }

    @Test
    public void testSampleTestBoxData() throws DemoExceptionResponse, IOException {

        genericResponseTransformer.setSpannerDatabaseService(spannerDatabaseService);

        stubFor(get(anyUrl())
                .willReturn(aResponse()
                        .withTransformerParameter(SpannerConstants.TABLE_NAME, SpannerConstants.SAMPLE_TEST_BOX_TABLE_NAME)
                        .withTransformerParameter(SpannerConstants.DATABASE_NAME, SpannerConstants.DATABASE_FOUR)
                        .withTransformers(GENERIC_RESPONSE_TRANSFORMER)));

        SampleTestBoxData sampleTestBoxData = demoService.getSampleTestBoxData();
        log.info("sampleTestBoxData received : {}", sampleTestBoxData);

        Assert.assertEquals(sampleTestBoxData.getSampleData(), "test-data-this-is");
    }


}
