package com.spanner.googlespanner.wiremock;

import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder;
import com.github.tomakehurst.wiremock.common.FileSource;
import com.github.tomakehurst.wiremock.extension.Parameters;
import com.github.tomakehurst.wiremock.extension.ResponseDefinitionTransformer;
import com.github.tomakehurst.wiremock.http.Request;
import com.github.tomakehurst.wiremock.http.ResponseDefinition;
import com.spanner.googlespanner.constant.SpannerConstants;
import com.spanner.googlespanner.spanner.SpannerDatabaseService;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;


@Slf4j
@Component
@Setter
public class GenericResponseTransformer extends ResponseDefinitionTransformer {

    private static final String GENERIC_RESPONSE_TRANSFORMER = "generic-response-transformer";

    private SpannerDatabaseService spannerDatabaseService;

    @SneakyThrows
    @Override
    public ResponseDefinition transform(Request request, ResponseDefinition responseDefinition, FileSource fileSource, Parameters parameters) {

        String tableName = parameters.getString(SpannerConstants.TABLE_NAME);
        String databaseName = parameters.getString(SpannerConstants.DATABASE_NAME);
        String responseData = null;

        log.info("table passed in request parameters are table : {} , database : {} ", tableName , databaseName);

        if (tableName.equals(SpannerConstants.OUTBOX_TABLE_NAME)) {

            log.info("matched - OUTBOX");
            responseData = spannerDatabaseService.retrieveByTableOutbox(tableName,databaseName);

        } else if (tableName.equals(SpannerConstants.INBOX_TABLE_NAME)) {

            log.info("matched - INBOX");
            responseData = spannerDatabaseService.retrieveByTableInboxData(tableName,databaseName);

        } else if (tableName.equals(SpannerConstants.SAMPLE_TEST_BOX_TABLE_NAME)) {

            log.info("matched - SAMPLE_TEST_BOX");
            responseData = spannerDatabaseService.retrieveByTableSampleTestBox(tableName,databaseName);
        }

        log.info("response data : {}", responseData);

        return new ResponseDefinitionBuilder()
                .withStatus(HttpStatus.OK.value())
                .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .withBody(responseData)
                .build();
    }

    @Override
    public String getName() {
        return GENERIC_RESPONSE_TRANSFORMER;
    }
}
