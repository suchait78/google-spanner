package com.spanner.googlespanner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GoogleSpannerApplication {

	public static void main(String[] args) {
		SpringApplication.run(GoogleSpannerApplication.class, args);
	}

}
