package com.spanner.googlespanner.service;

import com.spanner.googlespanner.constant.SpannerConstants;
import com.spanner.googlespanner.dto.DataSet;
import com.spanner.googlespanner.dto.InboxData;
import com.spanner.googlespanner.dto.OutboxData;
import com.spanner.googlespanner.dto.SampleTestBoxData;
import com.spanner.googlespanner.exception.DemoExceptionResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import java.util.List;

@Slf4j
@Service
public class DemoService {

    @Autowired
    private WebClient webClient;

    public List<DataSet> getAllJobs() throws DemoExceptionResponse {

        log.info("Fetching all jobs available from job-service.");

        try {
            return webClient.get().uri(SpannerConstants.ANY_API_V1)
                    .retrieve()
                    .bodyToFlux(DataSet.class)
                    .collectList()
                    .block();
        } catch (WebClientResponseException ex) {
            log.error("Error occurred while calling job-service API. {}, {}", ex.getStatusText(), ex.getResponseBodyAsString());
            throw new DemoExceptionResponse(ex.getStatusText(), ex);
        } catch (Exception ex) {
            log.error("Exception occurred , {}", ex.getMessage());
            throw new DemoExceptionResponse(ex);
        }
    }

    public OutboxData getOutboxData() throws DemoExceptionResponse {

        log.info("Fetching all jobs available from job-service.");

        try {
            return webClient.get().uri(SpannerConstants.OUTBOX_API_V1)
                    .retrieve()
                    .bodyToMono(OutboxData.class)
                    .block();
        } catch (WebClientResponseException ex) {
            log.error("Error occurred while calling job-service API. {}, {}", ex.getStatusText(), ex.getResponseBodyAsString());
            throw new DemoExceptionResponse(ex.getStatusText(), ex);
        } catch (Exception ex) {
            log.error("Exception occurred , {}", ex.getMessage());
            throw new DemoExceptionResponse(ex);
        }
    }

    public InboxData getInboxData() throws DemoExceptionResponse {

        log.info("Fetching all jobs available from job-service.");

        try {
            return webClient.get().uri(SpannerConstants.INBOX_API_V1)
                    .retrieve()
                    .bodyToMono(InboxData.class)
                    .block();
        } catch (WebClientResponseException ex) {
            log.error("Error occurred while calling job-service API. {}, {}", ex.getStatusText(), ex.getResponseBodyAsString());
            throw new DemoExceptionResponse(ex.getStatusText(), ex);
        } catch (Exception ex) {
            log.error("Exception occurred , {}", ex.getMessage());
            throw new DemoExceptionResponse(ex);
        }
    }

    public SampleTestBoxData getSampleTestBoxData() throws DemoExceptionResponse {

        log.info("Fetching all jobs available from job-service.");

        try {
            return webClient.get().uri(SpannerConstants.INBOX_API_V1)
                    .retrieve()
                    .bodyToMono(SampleTestBoxData.class)
                    .block();
        } catch (WebClientResponseException ex) {
            log.error("Error occurred while calling job-service API. {}, {}", ex.getStatusText(), ex.getResponseBodyAsString());
            throw new DemoExceptionResponse(ex.getStatusText(), ex);
        } catch (Exception ex) {
            log.error("Exception occurred , {}", ex.getMessage());
            throw new DemoExceptionResponse(ex);
        }
    }

}
