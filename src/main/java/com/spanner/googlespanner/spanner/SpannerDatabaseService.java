package com.spanner.googlespanner.spanner;

import org.springframework.stereotype.Service;
import wiremock.net.minidev.json.JSONObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


@Service
public class SpannerDatabaseService {

    private Connection getConnecton(String tableName, String databaseName) {

        SpannerDatabaseConnectionGeneric spannerDatabaseConnectionGeneric =
                new SpannerDatabaseConnectionGeneric(databaseName,"instance-1","spanner-demo");

        Connection connection = spannerDatabaseConnectionGeneric.getConnection();

        return connection;
    }

    private String createQuery(String tableName) {
        return "SELECT * from " + tableName;
    }

    public String retrieveByTableOutbox(String tableName, String databaseName) throws SQLException {

        String query = createQuery(tableName);

        Connection connection = getConnecton(tableName, databaseName);

        PreparedStatement ps
                = connection.prepareStatement(query);
        ResultSet rs = ps.executeQuery();
        String resp = "";
        while (rs.next()) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("locator",rs.getString("locator"));
            jsonObject.put("version",rs.getInt("version"));
            jsonObject.put("parent_locator",rs.getString("parent_locator"));
            jsonObject.put("created",rs.getString("created"));
            jsonObject.put("data",rs.getString("data"));
            jsonObject.put("status",rs.getInt("status"));
            jsonObject.put("retry_count",rs.getInt("retry_count"));
            jsonObject.put("updated",rs.getString("updated"));
            jsonObject.put("processing_time_millis",rs.getInt("processing_time_millis"));
            resp = jsonObject.toString();

        }
        return resp;
    }

    public String retrieveByTableInboxData(String tableName, String databaseName) throws SQLException {

        String query = createQuery(tableName);

        Connection connection = getConnecton(tableName, databaseName);
        PreparedStatement ps
                = connection.prepareStatement(query);
        ResultSet rs = ps.executeQuery();
        String resp = "";
        while (rs.next()) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("locator",rs.getString("locator"));
            jsonObject.put("version",rs.getInt("version"));
            jsonObject.put("parent_locator",rs.getString("parent_locator"));
            jsonObject.put("created",rs.getString("created"));
            jsonObject.put("data",rs.getString("data"));
            jsonObject.put("status",rs.getInt("status"));
            jsonObject.put("retry_count",rs.getInt("retry_count"));
            jsonObject.put("updated",rs.getString("updated"));
            jsonObject.put("processing_time_millis",rs.getInt("processing_time_millis"));
            resp = jsonObject.toString();

        }
        return resp;
    }

    public String retrieveByTableSampleTestBox(String tableName, String databaseName) throws SQLException {

        String query = createQuery(tableName);

        Connection connection = getConnecton(tableName, databaseName);

        PreparedStatement ps
                = connection.prepareStatement(query);
        ResultSet rs = ps.executeQuery();

        String resp = "";
        while (rs.next()) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("locator",rs.getString("locator"));
            jsonObject.put("version",rs.getInt("version"));
            jsonObject.put("created",rs.getString("created"));
            jsonObject.put("sample_data",rs.getString("sample_data"));
            jsonObject.put("status",rs.getInt("status"));
            jsonObject.put("updated",rs.getString("updated"));
            resp = jsonObject.toString();

        }
        return resp;
    }


}
