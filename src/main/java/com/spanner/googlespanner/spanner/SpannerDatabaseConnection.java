package com.spanner.googlespanner.spanner;

import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Slf4j
public class SpannerDatabaseConnection {

    private static Connection con = null;

    static {
        String project = "spanner-demo";
        String instanceId = "instance-1";
        String databaseId = "database-1";

        String url = "jdbc:cloudspanner://localhost:9010/projects/" + project +
                "/instances/" + instanceId + "/databases/" + databaseId + ";usePlainText=true";

        log.info("database url : {}", url);

        try {
            con = DriverManager.getConnection(url);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }


    public static Connection getConnection() {
        return con;
    }

}
