package com.spanner.googlespanner.spanner;

import com.google.cloud.spanner.Spanner;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Slf4j
@Setter
@Getter
public class SpannerDatabaseConnectionGeneric {

    private Connection connection;
    private String databaseId;
    private String instanceId;
    private String project;

    public SpannerDatabaseConnectionGeneric(String databaseId, String instanceId, String project) {
        this.databaseId = databaseId;
        this.instanceId = instanceId;
        this.project = project;
    }

    public Connection getConnection() {

        String url = "jdbc:cloudspanner://localhost:9010/projects/" + this.project +
                "/instances/" + this.instanceId + "/databases/" + this.databaseId + ";usePlainText=true";

        log.info("database url : {}", url);

        try {
            this.connection = DriverManager.getConnection(url);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return this.connection;
    }

    public void closeConnection() throws SQLException {
        this.connection.close();
    }


}
