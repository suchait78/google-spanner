Scope of improvement in this project ?

We need implements CommandLineRunner interface in SpringBootApplication class and we will get access to run method.
Inside run method
    - create different databases in spanner
    - insert database in databases
    - then this pre-requiste will go away.

Pre-requiste

Local Cloud Spanner Emulator Setup.

1. Install Docker
2. Docker should be running on the machine (can be verified using - 'docker run hello-world' command) and docker should be in the system path.
3. gcloud components update
4. gcloud emulators spanner start

The following message should be displayed:
“”””[cloud-spanner-emulator] 2021/05/04 08:48:51 gateway.go:140: Cloud Spanner emulator running.
[cloud-spanner-emulator] 2021/05/04 08:48:51 gateway.go:141: REST server listening at 0.0.0.0:9020
[cloud-spanner-emulator] 2021/05/04 08:48:51 gateway.go:142: gRPC server listening at 0.0.0.0:9010
“””””
Now, please open another command prompt:

5. Use any of the below two commands:
export SPANNER_EMULATOR_HOST="localhost:9010” Or gcloud emulators spanner env-init
6. a. gcloud config configurations create emulator
    b. gcloud spanner instance-configs list.  -> this should give emulator running in the list.

7. gcloud config set auth/disable_credentials true
8. gcloud config set project spanner-demo
9. gcloud config set api_endpoint_overrides/spanner http://localhost:9020/
10. Creating instance:
gcloud spanner instances create instance-1 --config=emulator-config --description="Test Instance" --nodes=1

9. Creating database with ddl:
gcloud spanner databases create database-1 --instance instance-1 --ddl "CREATE TABLE OUTBOX (  locator STRING(6) NOT NULL,  version INT64 NOT NULL,  parent_locator STRING(6),  created TIMESTAMP,  data STRING(MAX) NOT NULL,  status INT64 NOT NULL,  retry_count INT64,  updated TIMESTAMP OPTIONS (    allow_commit_timestamp = true  ),  processing_time_millis INT64,) PRIMARY KEY(locator, version);

gcloud spanner databases create database-2 --instance instance-1 --ddl "CREATE TABLE INBOX (  locator STRING(6) NOT NULL,  version INT64 NOT NULL,  parent_locator STRING(6),  created TIMESTAMP,  data STRING(MAX) NOT NULL,  status INT64 NOT NULL,  retry_count INT64,  updated TIMESTAMP OPTIONS (    allow_commit_timestamp = true  ),  processing_time_millis INT64,) PRIMARY KEY(locator, version);

gcloud spanner databases create database-4 --instance instance-1 --ddl "CREATE TABLE SAMPLE_TEST_BOX ( locator STRING(6) NOT NULL, version INT64 NOT NULL,created TIMESTAMP, sample_data STRING(MAX) NOT NULL, status INT64 NOT NULL, updated TIMESTAMP OPTIONS ( allow_commit_timestamp = true ), processing_time_millis INT64,) PRIMARY KEY(locator, version);


10. Inserting rows in DB:
gcloud spanner rows insert --table=OUTBOX --database=database-1 --instance=instance-1 --data=locator=ABC001,version=1,parent_locator=,created=NULL,data=data,status=0,retry_count=0,updated=NULL

gcloud spanner rows insert --table=INBOX --database=database-2 --instance=instance-1 --data=locator=ABC001,version=1,parent_locator=,created=NULL,data=data,status=0,retry_count=0,updated=NULL

gcloud spanner rows insert --table=SAMPLE_TEST_BOX --database=database-4 --instance=instance-1 --data=locator=TES001,version=1,created=NULL,sample_data=test-data-this-is,status=0,updated=NULL

11. retrieve data from DB:
gcloud spanner databases execute-sql database-2 --instance instance-1 --sql "select * from INBOX"
